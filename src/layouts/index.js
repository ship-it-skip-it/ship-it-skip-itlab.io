import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'

import './index.css'

const Header = () => (
  <div>
    <h1>
      <Link to="/">
        Ship It or Skip It
      </Link>
    </h1>
  </div>
)

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Ship It or Skip It"
      meta={[
        { name: 'description', content: 'home page for skip it or ship it' },
        { name: 'keywords', content: 'romcom, romance, comedy, movie, podcast, nagee, alex lee' },
      ]}
    />
    <Header />
    <div>
      {children()}
    </div>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
