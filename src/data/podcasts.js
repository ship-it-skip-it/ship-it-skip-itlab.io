const rssUrl = 'https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Ffeeds.soundcloud.com%2Fusers%2Fsoundcloud%3Ausers%3A459265953%2Fsounds.rss'

export default () => fetch(rssUrl).then(response => response.json())
