import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>hi people</h1>
    <p>welcome to our new podcast!</p>
    <p>
      <Link to="/episodes/">episodes</Link> are posted monthly
    </p>
    <p>
      follow us on twitter{' '}
      <a href="https://twitter.com/shipitskipit">@shipitskipit</a>
    </p>
  </div>
)

export default IndexPage
