import React, { Component } from 'react'
import Link from 'gatsby-link'

import getPodcasts from '../data/podcasts'
import { PodcastList } from '../components'

const Podcasts = ({ podcasts, image }) => (
  <PodcastList podcasts={podcasts} image={image} />
)

const PodcastsLoading = () => (
  <div> loading... </div>
)

const views = {
  LOADING: 'views/loading',
  PODCASTS: 'views/podcasts',
  ERROR: 'views/error',
}

const actions = {
  RELOAD_PODCASTS: 'actions/reload-podcasts',
  SHOW_PODCASTS: 'actions/show-podcasts',
  ERROR: 'actions/error',
}

const machine = {
  [views.PODCASTS]: {
    [actions.RELOAD_PODCASTS]: views.LOADING,
  },
  [views.ERROR]: {
    [actions.RELOAD_PODCASTS]: views.LOADING,
  },
  [views.LOADING]: {
    [actions.SHOW_PODCASTS]: views.PODCASTS,
    [actions.ERROR]: views.ERROR,
  },
}

const transition = (state, action) => machine[state][action]

class EpisodesWrapper extends Component {
  constructor() {
    super()
    this.state = {
      view: views.LOADING,
      podcasts: null,
      errorMessage: null,
      image: null,
    }
  }

  componentDidMount() {
    this.fetchPodcasts()
  }

  command = (action) => {
    console.log(action);
    switch (action.type) {
      case actions.RELOAD_PODCASTS:
        return { podcasts: null }
      case actions.SHOW_PODCASTS:
        return { podcasts: action.podcasts, image: action.image }
      case actions.ERROR:
        return { podcasts: null, errorMessage: action.errorMessage }
      default:
        return {}
    }
  }

  nextState = (action) => {
    const { view } = this.state;
    const nextView = transition(view, action.type);
    console.log(`${view} -> ${nextView}`)
    this.setState({
      view: nextView,
      ...this.command(action)
    })
  }

  fetchPodcasts() {
    getPodcasts()
      .then(json => {
        if (json.status !== 'ok')
          throw new Error('Could not load podcasts')
        console.log(json)
        const { items, feed } = json
        this.nextState({
          type: actions.SHOW_PODCASTS,
          podcasts: items,
          image: feed.image,
        })
      })
      .catch(e => 
        this.nextState({
          type: actions.ERROR,
          errorMessage: e.message,
        })
      )
  }

  render() {
    const { view, podcasts, errorMessage, image } = this.state
    const showView = (_view, component) => view === _view && component
    return (
      <div>
        <h1>Episodes</h1>
        {showView(views.LOADING, <PodcastsLoading />)}
        {showView(views.PODCASTS, <Podcasts podcasts={podcasts} image={image}  />)}
        {showView(views.ERROR, <div>{errorMessage}</div>)}
        {view || "view not defined"}
        <Link to="/">Go back to the homepage</Link>
      </div>
    )
  }
}

export default EpisodesWrapper 
