export default class Podcast {
  constructor({ name, number, link }) {
    this.episodeNumber = number
    this.episodeLink = link
    this.name = name
  }
}
