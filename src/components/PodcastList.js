import React, { Component } from 'react'
import styled from 'styled-components'
import { PodcastLink } from './PodcastLink'

export class PodcastList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm: '',
    }
  }

  handleSearchUpdate = (e) => {
    this.setState({
      searchTerm: e.target.value
    })
  }

  renderList() {
    const { podcasts } = this.props
    const { searchTerm } = this.state

    const podcastList = (podcasts) => podcasts.map((podcast, k) => <PodcastLink podcast={podcast} key={k} />)

    if (searchTerm) {
      const ps = this.props.podcasts.filter(p => p.title.toLowerCase().search(searchTerm.toLowerCase()) !== -1);
      if (ps.length === 0) return <div>No results</div>
      return podcastList(ps);
    }
    return podcastList(this.props.podcasts);
  }

  render() {
    const { image } = this.props;
    return (
      <div>
        <img style={{ height: '100px', width: '100px' }} src={image} />
        <input onChange={this.handleSearchUpdate} />
        {this.renderList()}
      </div>
    )
  }
}
