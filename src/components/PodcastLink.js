import React, { Component } from 'react'
import ReactPlayer from 'react-player'
import styled from 'styled-components'


const PodcastWrapper = styled.article`
  padding: 0.2rem;
`
const PodcastUrl = styled.a`
  color: pink;
`

const PodcastList = styled.ul`
  list-style-type: none;
`

const PodcastTitle = styled.h1`
  font-size: 1.2rem;
`

const views = {
  PLAYER: 'views/player',
  NO_PLAYER: 'views/no-player',
}

const actions = {
  show_player: 'actions/show-player',
  hide_player: 'actions/hide-player',
}

const machine = {
  [views.PLAYER]: {
    [actions.hide_player]: views.NO_PLAYER,
  },
  [views.NO_PLAYER]: {
    [actions.show_player]: views.PLAYER,
  }
}

export class PodcastLink extends Component {
  constructor(props) {
    super()
    this.state = {
      view: views.NO_PLAYER,
    }
  }

  nextView = (action) => {
    const { view } = this.state
    const nextView = machine[view][action.type]
    this.setState({ view: nextView })
  }

  handleClosePlayer = () => this.nextView({ type: actions.hide_player })

  handleOpenPlayer = () => this.nextView({ type: actions.show_player })

  renderButton(view) {
    if (view === views.PLAYER) {
      return (<button onClick={this.handleClosePlayer}>close</button>)
    }
    return (<button onClick={this.handleOpenPlayer}>open</button>)
  }

  render() {
    const { view } = this.state
    const { link, title } = this.props.podcast
    const showView = (v, c) => v === view && c
    return (
      <PodcastWrapper>
        <PodcastTitle>
          {this.renderButton(view)}
          {showView(views.PLAYER, <ReactPlayer url={link} />)}
          {showView(views.NO_PLAYER, <PodcastUrl href={link}>{title}</PodcastUrl>)}
        </PodcastTitle>
      </PodcastWrapper>
    )
  }
}


